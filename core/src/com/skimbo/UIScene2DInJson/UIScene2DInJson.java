package com.skimbo.UIScene2DInJson;

import java.util.HashMap;
import java.util.Map.Entry;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Json;
import com.skimbo.UIScene2DInJson.uijsondata.UILayout;
import com.skimbo.UIScene2DInJson.uijsondata.UILayout.InsideData;
import com.skimbo.UIScene2DInJson.uijsondata.UILayout.TextButtonData;

/**
 * Project: UIScene2DInJson-core
 * File: UIScene2DInJson.java
 * Created by Skimbo on 2.1.2015
 **/

public class UIScene2DInJson extends ApplicationAdapter {
	final String firstText = 
			"Here is something like a dialog that spawns " +
			"over several lines, and uses the wrap to actually go " +
			"into another line. It doesnt really make any sense what reads " +
			"in here, but at least it is possible to do something like this when " +
			"using wrap modifier.\n Pressing next will go to next one!";
	final String secondText = 
			"It changed!\n You can go back by pressing Back!";
	
	private int _dialogIndex = 0;

	Stage _stage = null;

			
	UILayout _layout = null;
	Skin _skin = null;
	String _jsonString = "";
	
	
	boolean _ctrlDown = false;
	boolean _reload = false;

	FreeTypeFontGenerator _generator = null;
	private HashMap<String, BitmapFont> _fonts = new HashMap<String, BitmapFont>();

	@Override
	public void create() {
		super.create();

		_generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/DejaVuSans-Bold.ttf"));
		
		_skin = new Skin();
		_skin.addRegions(new TextureAtlas(Gdx.files.internal("uiskin.atlas")));
		
		completeInit();
	}
	private void completeInit() {
		
		final Json json = new Json();
		System.out.println("parsing json");
		if( _jsonString.isEmpty())
			_layout = json.fromJson(UILayout.class, Gdx.files.internal("jsons/exampleui.json"));
		else
			_layout = json.fromJson(UILayout.class, _jsonString );
		System.out.println("parsed json");
		

		addFonts();
		

		
		System.out.println("skin loaded");
		
		_layout.setSkin(_skin);

		// After setting skin, we would need to reinit, but since we are
		// changing the labels text later on, we do it after that.
		//_layout.reinit();


		
		
		// After changes, there is a need for reiniting.
		_layout.getLabelData( "dialoglabel" ).text = firstText;
		_layout.reinit();

		_layout.printTree();
		
		_layout.getTextButtonData( "nextdialogbutton" ).button.addListener( new ClickListener()  {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if( _dialogIndex == 0 ) {
					_layout.getLabelData( "dialoglabel" ).text = secondText;
					_layout.reinit();
					++_dialogIndex;
				}
					
			}
		});
		
		_layout.getTextButtonData( "backdialogbutton" ).button.addListener( new ClickListener()  {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				if( _dialogIndex == 1 ) {
					_layout.getLabelData( "dialoglabel" ).text = firstText;
					_layout.reinit();
					--_dialogIndex;
				}
					
			}
		});
		
		_layout.getTextButtonData("addmorebutton").button.addListener( new ClickListener () {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				System.out.println("Adding button!");
				
				InsideData data = new InsideData();
				data.pad.add(1f); // padding to 1
				data.row = true; // we want to change row after adding
				
				// Create a button with the name of "button". If name conflicts with buttons created before it will create
				// unique name. We set it inside topbuttontable, and set text to empty. Also we add it after others in table,
				// not in front.
				// The method uses change parent, which puts the newly created button inside topbuttontable. If we give empty string,
				// to the method, we can pop them out of groups, or with name we can set them into different groups/tables.
				TextButtonData buttonData =_layout.createNewTextButton("button", "topbuttontable", "", "small", false, data);
				buttonData.text = buttonData.name;
				
				//update the layout after changing it
				_layout.reinit();
			}
		});
		
		_layout.getTextButtonData("savebutton").button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				System.out.println( json.prettyPrint(_layout));
				_jsonString = json.toJson( _layout );

			}
		});
		_layout.getTextButtonData("loadbutton").button.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				_reload = true;
			}
		});

		init();
		
	}
	

	private void addFonts() {
		for( Entry<String, BitmapFont> entry : _fonts.entrySet() ) {
			entry.getValue().dispose();
		}
		_fonts.clear();
		
		float fontWeight = Math.min( Gdx.graphics.getWidth(), Gdx.graphics.getHeight() );
		
		
		_fonts.put( "small", loadFont( (int)( fontWeight / 36.f ) ));
		_fonts.put( "medium", loadFont( (int)( fontWeight / 24.f ) ));
		_fonts.put( "big", loadFont( (int)( fontWeight / 12.f ) ));
		
		for( Entry<String, BitmapFont> entry : _fonts.entrySet() ) {
			_skin.add( entry.getKey(), entry.getValue() );
		}
		
		// After changing fonts skin needs to be reloaded in order to actually
		// change the fonts into styles
		_skin.load(Gdx.files.internal("uiskin.json"));
	
	}
	
	private BitmapFont loadFont(int i) {
		
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = i;
		BitmapFont newFont = _generator.generateFont(parameter); 
		return newFont;
	}
	@Override
	public void resume() {

		addFonts();
		init();
	}
	
	@Override
	public void pause() {

	}
	@Override
	public void resize(int width, int height) {	
		System.out.println("resize called!");
		super.resize(width, height);
		addFonts();
		init();
	}
	@Override
	public void render() {
		if( _reload ) 
			completeInit();
		_reload = false;
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		
		super.render();
		_stage.act();
		
		_stage.draw();
	}
	@Override
	public void dispose() {
		_generator.dispose(); 
		
		for( Entry<String, BitmapFont> entry : _fonts.entrySet() ) {
			entry.getValue().dispose();
		}	
		_fonts.clear();

		_layout.dispose();
		_stage.dispose();
		
		super.dispose();
	}
	
	private void init() {
		if( _stage != null ) {
			_stage.dispose();
		}
		_stage = new Stage();
		//_stage.setDebugAll(true);
		
		
		_layout.reinit();
		
		

		_stage.addActor( _layout.getGroupData("uigroup").group);
		final Json json = new Json();

		_stage.addListener(new InputListener() {
			@Override
			public boolean keyUp(InputEvent event, int keycode) {
				switch( keycode ) {
				case Keys.CONTROL_LEFT:
				case Keys.CONTROL_RIGHT:
					_ctrlDown = false;
				break;
				
				default:
					return super.keyDown(event, keycode);
				}
				return true;
			}
			@Override
			public boolean keyDown(InputEvent event, int keycode) {
				switch( keycode ) {
					case Keys.CONTROL_LEFT:
					case Keys.CONTROL_RIGHT:
						_ctrlDown = true;
					break;

					case Keys.Q: 
						if ( _ctrlDown )
							Gdx.app.exit();
					break;
					case Keys.S:
						if( _ctrlDown ) {
							System.out.println( json.prettyPrint(_layout));
							_jsonString = json.toJson( _layout );

						}
						break;
					case Keys.L: 
						if( _ctrlDown ) {
							_reload  = true;
						}
					default:
						return super.keyDown(event, keycode);
						
				}
				return true;
			}
		});

		Gdx.input.setInputProcessor(_stage);
	}
	
}
