package com.skimbo.UIScene2DInJson.uijsondata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.JsonValue;


/**
 * Project: UIScene2DInJson-core
 * File: UILayout.java
 * Created by Skimbo on 2.1.2015
 **/

public class UILayout implements Serializable {
	
	static public class AllData {	
		public String name = "";
		public Vector2 pivot = new Vector2();
		public Vector2 pos = new Vector2();
		public boolean pack = true;
	}
	
	static public class MyUIHelperType {
		public String name = "";
		public ArrayList<String> children = new ArrayList<String>();
		public String type = "";
		public String parent = "";
	}

	static public class UIFontType {
		public String ttf = "";
		public HashMap< String, Float > names = new HashMap<String, Float>();		
	}
	
	
	static public class CommonContainerData extends AllData {
		public float height = -1f;
		public float width = -1f;
		public ArrayList<Float> pad = new ArrayList<Float>();		

		public CommonContainerData() {}

}

	static public class InsideData extends CommonContainerData{
		public boolean row = false;
		public boolean fillX = false;
		public boolean fillY = false;
		public boolean expandX = false;
		public boolean expandY = false;
		public int colspan = 1;
		public String align = "";
		
		public InsideData() {}

	}

	
	static public class CommonGroupData extends CommonContainerData {
		public LinkedHashMap<String, InsideData> objects = new LinkedHashMap<String, InsideData>();
		
	}
	static public class TableData extends CommonGroupData {
		public Table table;
		public String background = "";
		public String align = "";
	}
	
	static public class GroupData extends CommonGroupData {
		public Group group;
	}
	
	//////////////////////////////////////////////////////////////////////
	
	static public class TextData extends AllData {
		public String text = "";
		public String skin = "default";
		public boolean wrap = false;
	}

	static public class LabelData extends TextData {
		public Label label;		
		public String textalign = "";
	}
	
	static public class TextButtonData extends TextData {
		public TextButton button;
		public ArrayList< Float > pad = new ArrayList<Float>();
		public String textalign = "";
		public String align = "";
	}

	static public class TextFieldData extends TextData {
		public TextField field;
		public String messageText = "";
		
	}

	/////////////////////////////////////////////////////////////////
	
	
	static public class FontStyleData {
		public String fontType = "";
		public String fontSize = "";
		public Color color = Color.WHITE;	
	}

	static public class LabelStyleData extends FontStyleData {
		public LabelStyle style = new LabelStyle();
		public FontStyleData fontData = new FontStyleData();
		public String background = "";
	}

	static public class TextButtonStyleData extends FontStyleData {	
		public TextButtonStyle style = new TextButtonStyle();
		public String down = "";
		public String up = "";
		public String checked = "";
	}
	
	static public class TextFieldStyleData extends FontStyleData {
		public TextFieldStyle style = new TextFieldStyle();
		public String selection = "";
		public String focus = "";
		public String background = "";
		public String cursor = "";
	}
	
	
	BitmapFont _defaultFont = new BitmapFont();
	
	LabelStyle _labelStyle = new LabelStyle( _defaultFont, Color.WHITE );
	TextButtonStyle _buttonStyle = new TextButtonStyle();
	TextFieldStyle _textFieldStyle = new TextFieldStyle(_defaultFont, Color.WHITE, null, null, null );
	
	HashMap< String, TableData > tables = new HashMap<String, TableData>();
	HashMap< String, GroupData > groups = new HashMap<String, GroupData >();
	HashMap< String, LabelData > labels = new HashMap<String, LabelData>();
	HashMap< String, TextButtonData > textButtons = new HashMap<String, TextButtonData>();
	HashMap< String, TextFieldData > textFields = new HashMap<String, TextFieldData >();
	
	HashMap< String, MyUIHelperType > _allItems = new HashMap<String, MyUIHelperType>();
	ArrayList< MyUIHelperType > _roots = new ArrayList<MyUIHelperType>();
	
	Skin _skin;

	
	public UILayout() {
		_buttonStyle.font = _defaultFont;
		
		
		createRootGroup();
		
	}
	
	
	private void createRootGroup() {		
		createNewGroup( "theRootGroup", "", false, null );
	}
	
	public void createNewGroup( String name, String intoItem, boolean front, InsideData insideData ) {
		GroupData data = new GroupData();
		data.group = new Group();
		
		MyUIHelperType helper = getNewHelper(name, "Group");
		data.name = helper.name;

		
		groups.put(helper.name, data);

		changeParent(helper.name, intoItem, front, insideData );
	}

	public void createNewTable( String name, String intoItem, boolean front, InsideData insideData ) {
		TableData data = new TableData();
		data.table = new Table();
		
		MyUIHelperType helper = getNewHelper(name, "Table");
		data.name = helper.name;

		
		tables.put(helper.name, data);
		
		changeParent(helper.name, intoItem, front, insideData );
	}

	public void createNewLabel( String name, String intoItem, String text, String skin, boolean front, InsideData insideData ) {
		LabelData data = new LabelData();
		data.text = text;
		data.skin = skin;
		data.label = new Label( data.text, _skin, data.skin);
		
		MyUIHelperType helper = getNewHelper(name, "Label");
		data.name = helper.name;
		
		labels.put(helper.name, data);
		
		changeParent(helper.name, intoItem, front, insideData );
	}

	public TextButtonData createNewTextButton( String name, String intoItem, String text, String skin, boolean front, InsideData insideData ) {
		TextButtonData data = new TextButtonData();
		data.text = text;
		data.skin = skin;
		data.button = new TextButton( data.text, _skin, data.skin  );

		MyUIHelperType helper = getNewHelper(name, "TextButton");
		data.name = helper.name;
		
			
		textButtons.put(helper.name, data);
		
		changeParent(helper.name, intoItem, front, insideData );
		return data;
	}
	
	public void createNewTextField( String name, String intoItem, String text, String skin, boolean front, InsideData insideData ) {
		TextFieldData data = new TextFieldData();
		data.text = text;
		data.skin = skin;
		
		data.field = new TextField( data.text, _skin, data.skin );
		
		MyUIHelperType helper = getNewHelper(name, "TextField");
		data.name = helper.name;
		
				
		textFields.put(helper.name, data);
		
		changeParent(helper.name, intoItem, front, insideData );
	}


	private MyUIHelperType getNewHelper( String name, String nameType ) {
		MyUIHelperType helper = new MyUIHelperType();
		helper.name = getNewUniqueName(name, nameType );
		helper.type = nameType;
		_allItems.put( helper.name, helper );
		return helper;
	}
	private String getNewUniqueName( String name, String nameType ) {
		if( name.isEmpty() || _allItems.get(name) != null ) {
			int i = _allItems.size();
			while( _allItems.get( nameType + i ) != null ) ++i;
			name = nameType + i;
		}
		return name;
	}
	
	private void putInsideItem(String name, Actor actor, AllData data, String item) {
		Group group;
		CommonGroupData groupData;
		if( tables.get(item) != null ) {
			groupData = tables.get(item);
			group = tables.get(item).table;
		}
		else if( groups.get(item) != null  ) {
			group = groups.get( item ).group;
			groupData = groups.get(item);
		}
		else {
			return;
		}
		InsideData insideData = new InsideData();
		groupData.objects.put( name, insideData );
		addInner(group, name, insideData );
		
	}
	
	@Override
	public void write(Json json) {
		System.out.println("writing!");

		json.writeObjectStart("groups");
		for( Entry<String, GroupData> entry : groups.entrySet() ) {
			Group group = entry.getValue().group;
			entry.getValue().group = null;
			json.writeValue(entry.getKey(), entry.getValue());
			entry.getValue().group = group;
		}
		json.writeObjectEnd();

		json.writeObjectStart("tables");
		for( Entry<String, TableData> entry : tables.entrySet() ) {
			Table table = entry.getValue().table;
			entry.getValue().table = null;
			json.writeValue(entry.getKey(), entry.getValue());
			entry.getValue().table = table;
		}
		json.writeObjectEnd();
		
		json.writeObjectStart("textbuttons");
		for( Entry<String, TextButtonData> entry : textButtons.entrySet() ) {
			TextButton button = entry.getValue().button;
			entry.getValue().button = null;
			json.writeValue(entry.getKey(), entry.getValue());
			entry.getValue().button = button;
		}
		json.writeObjectEnd();

		json.writeObjectStart("labels");
		for( Entry<String, LabelData> entry : labels.entrySet() ) {
			Label label = entry.getValue().label;
			entry.getValue().label = null;
			json.writeValue(entry.getKey(), entry.getValue());
			entry.getValue().label = label;
		}
		json.writeObjectEnd();

		json.writeObjectStart("textFields");
		for( Entry<String, TextFieldData> entry : textFields.entrySet() ) {
			TextField field = entry.getValue().field;
			entry.getValue().field = null;
			json.writeValue(entry.getKey(), entry.getValue());
			entry.getValue().field = field;
		}
		json.writeObjectEnd();
		
	}
	@Override
	public void read(Json json, JsonValue jsonData) {
		System.out.println("Reading json!");
		dispose();
		
		_roots.clear();		
		_allItems.clear();

		JsonValue tableJson = jsonData.get("tables"); 				 
		JsonValue labelJsons = jsonData.get("labels");
		JsonValue buttonJsons = jsonData.get("textButtons");
		JsonValue groupJsons = jsonData.get("groups");
		JsonValue textFieldJsons = jsonData.get("textFields");
		

		if( tableJson != null ) tableJson = tableJson.child;
		if( labelJsons != null ) labelJsons = labelJsons.child;
		if( buttonJsons != null ) buttonJsons = buttonJsons.child;
		if( groupJsons != null ) groupJsons = groupJsons.child;
		if( textFieldJsons != null ) textFieldJsons = textFieldJsons.child;

		textButtons.clear();
		labels.clear();
		tables.clear();
		groups.clear();

		while( labelJsons != null ) {
			Label label = new Label( "", _labelStyle ); 
			LabelData data = json.readValue(LabelData.class, labelJsons);
			label.setText( data.text );
			data.label = label;
			label.setAlignment( getAlign(data.textalign));
			label.setWrap( data.wrap );
			
			MyUIHelperType helper = new MyUIHelperType();
			helper.name = labelJsons.name;
			helper.type = "Label";			
			_allItems.put( helper.name, helper);
			data.name = helper.name;
			
			label.setName( labelJsons.name );
			labels.put( labelJsons.name, data );
			labelJsons = labelJsons.next;
		}
		while( buttonJsons != null ) {
			TextButton button = new TextButton( "", _buttonStyle );			
			TextButtonData data = json.readValue(TextButtonData.class, buttonJsons);
			button.setText( data.text );
			data.button = button;
			addTablePads( button, data.pad, jsonData.name );
			button.align( getAlign(data.align )) ;
			button.getLabel().setAlignment(getAlign(data.textalign));
			button.getLabel().setWrap( data.wrap );
	
			MyUIHelperType helper = new MyUIHelperType();
			helper.name = buttonJsons.name;
			helper.type = "TextButton";
			_allItems.put( helper.name, helper);
			data.name = helper.name;

			
			button.setName(buttonJsons.name);
			textButtons.put( buttonJsons.name, data );
			buttonJsons = buttonJsons.next;
		}
		while( textFieldJsons != null ) {
			TextField textField = new TextField( "", _textFieldStyle );			
			TextFieldData data = json.readValue(TextFieldData.class, textFieldJsons );
			textField.setText( data.text );
			textField.setMessageText(data.messageText);
			data.field = textField;
	
			MyUIHelperType helper = new MyUIHelperType();
			helper.name = textFieldJsons.name;
			helper.type = "TextField";
			_allItems.put( helper.name, helper);
			data.name = helper.name;

			
			textField.setName( textFieldJsons.name );
			textFields.put( textFieldJsons.name, data );
			textFieldJsons = textFieldJsons.next;
		}

		while( tableJson != null ) {
			Table table = new Table();
			TableData tableData = json.readValue( TableData.class, tableJson );
			tableData.table = table;
			addTablePads( table, tableData.pad, jsonData.name );
			table.align( getAlign( tableData.align ) ); 

			MyUIHelperType helper = new MyUIHelperType();
			helper.name = tableJson.name;
			helper.type = "Table";
			_allItems.put( helper.name, helper);
			tableData.name = helper.name;

			
			table.setName( tableJson.name );
			tables.put( tableJson.name, tableData );
			tableJson = tableJson.next;
		}
		while( groupJsons != null ) {
			Group group = new Group();
			GroupData groupData = json.readValue(GroupData.class, groupJsons);
			groupData.group = group;
			
			
			MyUIHelperType helper = new MyUIHelperType();
			helper.name = groupJsons.name;
			helper.type = "Group";
			_allItems.put( helper.name, helper);
			groupData.name = helper.name;

			
			group.setName( groupJsons.name );
			groups.put(groupJsons.name, groupData );
			groupJsons = groupJsons.next;
		}
		
		// build the tree for items
		for( Entry<String, GroupData> entry : groups.entrySet() ) {
			MyUIHelperType currItem = _allItems.get( entry.getKey() );
			for( Entry<String, InsideData> entry2 : entry.getValue().objects.entrySet() ) {
				MyUIHelperType childItem = _allItems.get( entry2.getKey() );
				if( childItem == null ) {
					System.err.println( entry2.getKey() + " from group not defined!");
					Gdx.app.exit();
				}
				childItem.parent = entry.getKey();
				currItem.children.add( entry2.getKey() );
			}
		}

		for( Entry<String, TableData> entry : tables.entrySet() ) {
			MyUIHelperType currItem = _allItems.get( entry.getKey() );
			for( Entry<String, InsideData> entry2 : entry.getValue().objects.entrySet() ) {
				MyUIHelperType childItem = _allItems.get( entry2.getKey() );
				if( childItem == null ) {
					System.err.println( entry2.getKey() + " from table not defined!");
					Gdx.app.exit();
				}
				childItem.parent = entry.getKey();
				currItem.children.add( entry2.getKey() );
			}						
		}

		for( Entry<String, MyUIHelperType> entry : _allItems.entrySet() ) 
			if( entry.getValue().parent.isEmpty() )
				_roots.add( entry.getValue() );
		
		
	
	}

	private int getAlign( String align ) {
		if( align.equals("top") )
			return Align.top;
		else if( align.equals("topleft"))			
			return Align.topLeft;
		else if( align.equals("topright") )
			return Align.topRight;
		else if( align.equals("bottom") )
			return Align.bottom;
		else if( align.equals("bottomleft"))
			return Align.bottomLeft;
		else if( align.equals("bottomright") )
			return Align.bottomRight;
		else if( align.equals("left")) 
			return Align.left;
		else if(align.equals("right"))
			return Align.right;
		
		return Align.center;
				
	}
	public void changeParent( String actorName, String intoString, boolean front, InsideData insideData ) {
		if( actorName.equals(intoString)) {
			System.err.println("Cannot change parent to be itself!");
			return;
		}
		if( actorName.isEmpty() ) {
			System.err.println("You didn't give any item to remove.");
			return;
		}
		MyUIHelperType helper = _allItems.get(actorName);
		if( helper == null ) {
			System.err.println( actorName + " is not defined anywhere.");
			return;
		}
		MyUIHelperType newParentHelper = null;

		Group group = null;
		CommonGroupData groupData = null;
		
		if( !intoString.isEmpty() ) {
			newParentHelper = _allItems.get( intoString );
			if( newParentHelper == null ) {
				System.err.println( intoString + " is not defined anywhere. Cannot change parent into this.");
				return;
			}
			groupData = tables.get( intoString );
			if( groupData == null ) {
				groupData = groups.get( intoString );
				if( group == null ) {
					System.err.println( intoString + " is not found in tables nor groups.");
					return;
				}
				group = groups.get( intoString ).group;
			}
			else {
				group = tables.get( intoString ).table;
			}
			

		}
		
		MyUIHelperType oldParentHelper;
		if( !helper.parent.isEmpty() ) {
			oldParentHelper = _allItems.get( helper.parent );
			helper.parent = "";
			int i = 0;
			while( i < oldParentHelper.children.size() ) {
				if( oldParentHelper.children.get(i).equals(actorName) ) {
					oldParentHelper.children.remove(i);
					break;
				}
				++i;
			}
			CommonGroupData oldGroupData = tables.get( oldParentHelper.name );
			if( oldGroupData == null ) {
				oldGroupData = groups.get( oldParentHelper.name );
			}
			//insideData = oldGroupData.objects.remove( actorName );
			oldGroupData.objects.remove( actorName );
		}
		else {
			_roots.remove(helper);
		}

		if( tables.get(actorName) != null ) {
			tables.get(actorName).table.remove();			
		}
		else if( groups.get( actorName) != null ) {
			groups.get(actorName).group.remove();						
		}
		else if( textButtons.get( actorName ) != null ) {
			textButtons.get(actorName).button.remove();						
		}			
		else if( textFields.get( actorName ) != null ) {
			textFields.get(actorName).field.remove();						
		}			
		else if( labels.get( actorName ) != null ) {
			labels.get(actorName).label.remove();						
		}			
	
		
		if( group != null ) {
			if( insideData == null )
				insideData = new InsideData();
			if( front ) {
				LinkedHashMap<String, InsideData> newmap=(LinkedHashMap<String, InsideData>) groupData.objects.clone();
				groupData.objects.clear();
				groupData.objects.put(actorName, insideData);
				groupData.objects.putAll(newmap);
				newParentHelper.children.add(0, actorName);
			}
			else {
				groupData.objects.put(actorName, insideData);
				newParentHelper.children.add(actorName);
			}
			helper.parent = intoString;
		}
		else {
			if( front )
				_roots.add(0, helper);
			else
				_roots.add(helper);
		}
	}

	private void addInner(Group group, String nameString, InsideData insideData ) {
		if( labels.get(nameString) != null ) {
			groupAdd( group, labels.get(nameString).label, insideData );
		}
		else if( textButtons.get(nameString) != null ) {
			groupAdd( group, textButtons.get(nameString).button, insideData );
		}
		else if( tables.get(nameString) != null ) {
			groupAdd( group, tables.get(nameString).table, insideData );
		}
		else if( groups.get(nameString) != null ) {
			groupAdd( group, groups.get(nameString).group, insideData );
		}
		else if( textFields.get(nameString) != null ) {
			groupAdd( group, textFields.get(nameString).field, insideData );			
		}
		else {
			System.err.println("Tables actor isn't defined in the json file: " + nameString);
			Gdx.app.exit();
		}
		
		
	}
	private void groupAdd(Group group, Actor actor, InsideData insideData) {

		if( group.getClass().equals(Table.class) ) {
			Cell cell = ((Table) group).add( actor );			
			if( insideData.width >= 0 )
				cell.width( insideData.width * Gdx.graphics.getWidth() );
			if( insideData.height >= 0 )
				cell.height( insideData.height * Gdx.graphics.getHeight() );
			cell.fill( insideData.fillX, insideData.fillY);
			cell.expand(insideData.expandX, insideData.expandY);
			cell.colspan(insideData.colspan);
			cell.align(getAlign(insideData.align));
			addCellPads(cell, insideData.pad,"on adding to table");
		}
		else {
			group.addActor( actor );
		}
	}
	private void addCellPads(Cell cell, ArrayList<Float> pad, String string) {
		if( pad.size() == 0 )
			return;
		else if( pad.size() == 1 ) {
			cell.pad(pad.get(0));
			
		}
		else if( pad.size() == 4) {
			cell.pad( pad.get(0), pad.get(1), pad.get(2), pad.get(3) );
		}
		else {
			System.err.println("Only use either 0,1 or 4 pads in json: " + string );
			Gdx.app.exit();
		}
		
	}

	private void addTablePads(Table table, ArrayList<Float> pads, String s ) {
		if( pads.size() == 0 )
			return;
		else if( pads.size() == 1 ) {
			table.pad(pads.get(0));
			
		}
		else if( pads.size() == 4) {
			table.pad( pads.get(0), pads.get(1), pads.get(2), pads.get(3) );
		}
		else {
			System.err.println("Only use either 0,1 or 4 pads in json: " + s );
			Gdx.app.exit();
		}
	}
	
	public void reinit() {
		for( LabelData data : labels.values() ) {			
			data.label.setStyle( _skin.get(data.skin, LabelStyle.class));
			data.label.setText( data.text );

			if( data.pack )
				data.label.pack();

			data.label.setPosition( Gdx.graphics.getWidth() * data.pos.x, 
				Gdx.graphics.getHeight() * data.pos.y );
					
			data.label.setOrigin(-data.pivot.x * data.label.getWidth(), 
				-data.pivot.y * data.label.getHeight());

			data.label.moveBy( -data.pivot.x * data.label.getWidth(), 
				-data.pivot.y * data.label.getHeight() );			
		}
		
		for( TextButtonData data : textButtons.values() ) {
			data.button.setStyle( _skin.get(data.skin, TextButtonStyle.class));
			data.button.setText( data.text );
			
			if( data.pack )
				data.button.pack();

			data.button.setPosition( Gdx.graphics.getWidth() * data.pos.x, 
				Gdx.graphics.getHeight() * data.pos.y );
					
			data.button.setOrigin(-data.pivot.x * data.button.getWidth(), 
				-data.pivot.y * data.button.getHeight());

			data.button.moveBy( -data.pivot.x * data.button.getWidth(), 
				-data.pivot.y * data.button.getHeight() );

		}
		for( TextFieldData data : textFields.values()) {
			data.field.setStyle( _skin.get(data.skin, TextFieldStyle.class));
			data.field.setText( data.text );
			
			if( data.pack )
				data.field.pack();

			data.field.setPosition( Gdx.graphics.getWidth() * data.pos.x, 
				Gdx.graphics.getHeight() * data.pos.y );
				
			data.field.setOrigin(-data.pivot.x * data.field.getWidth(), 
				-data.pivot.y * data.field.getHeight());

			data.field.moveBy( -data.pivot.x * data.field.getWidth(), 
				-data.pivot.y * data.field.getHeight() );
			
		}
		
		for( TableData tableData : tables.values() ) {
			tableData.table.clear();
		}
		for( GroupData groupData : groups.values() ) {
			groupData.group.clear();
		}

		populateTables();
		populateGroups();
	}
	

	private void populateGroups() {
		
	for( Entry<String, GroupData> entry : groups.entrySet() ) {
		GroupData groupData = entry.getValue();
	
		for( Entry<String, InsideData> entry2 : groupData.objects.entrySet() ) {
			addInner(groupData.group, entry2.getKey(), entry2.getValue() );
		}
		
		if( groupData.width >= 0f )
			groupData.group.setWidth( groupData.width * Gdx.graphics.getWidth() );
		if( groupData.height >= 0f )
			groupData.group.setHeight( groupData.height * Gdx.graphics.getHeight() );
	
		
		
		groupData.group.setPosition( Gdx.graphics.getWidth() * groupData.pos.x, 
			Gdx.graphics.getHeight() * groupData.pos.y );
		
		groupData.group.setOrigin(-groupData.pivot.x * groupData.group.getWidth(), 
				-groupData.pivot.y * groupData.group.getHeight());

		groupData.group.moveBy( -groupData.pivot.x * groupData.group.getWidth(), 
				-groupData.pivot.y * groupData.group.getHeight() );
	
		}

	}

	private void populateTables() {
		for( Entry<String, TableData> entry : tables.entrySet() ) {
			TableData tableData = entry.getValue();
		
			if( !tableData.background.isEmpty())
				tableData.table.setBackground( _skin.getDrawable( tableData.background));
			for( Entry<String, InsideData> entry2 : tableData.objects.entrySet() ) {
				
				addInner(tableData.table, entry2.getKey(), entry2.getValue() );				
				
				if(entry2.getValue().row) 
					tableData.table.add().row();
			}
			
			
			if( tableData.pack )
				tableData.table.pack();
			
			
			if( tableData.width >= 0f )
				tableData.table.setWidth( tableData.width * Gdx.graphics.getWidth() );
			if( tableData.height >= 0f )
				tableData.table.setHeight( tableData.height * Gdx.graphics.getHeight() );
			
			tableData.table.setPosition( Gdx.graphics.getWidth() * tableData.pos.x, 
					Gdx.graphics.getHeight() * tableData.pos.y );
			tableData.table.setOrigin(tableData.pivot.x * tableData.table.getWidth(), 
					tableData.pivot.y * tableData.table.getHeight());
			tableData.table.moveBy( -tableData.pivot.x * tableData.table.getWidth(), 
					-tableData.pivot.y * tableData.table.getHeight() );

			
		}

	}

	public ArrayList< MyUIHelperType > getRootHelpers() {
		return _roots;
	}

	

	public ArrayList<String> getGroupNames() {
		ArrayList< String > names = new ArrayList<String>();
		for( Entry<String, GroupData> entry : groups.entrySet() ) {
			names.add(entry.getKey());
		}
		return names;
	}

	public ArrayList<String> getTextButtonNames() {
		ArrayList< String > names = new ArrayList<String>();
		for( Entry<String, TextButtonData> entry : textButtons.entrySet() ) {
			names.add(entry.getKey());
		}
		return names;
	}
	
	public ArrayList<String> getLabelNames() {
		ArrayList< String > names = new ArrayList<String>();
		for( Entry<String, LabelData> entry : labels.entrySet() ) {
			names.add(entry.getKey());
		}
		return names;
	}

	public ArrayList<String> getTextFieldNames() {
		ArrayList< String > names = new ArrayList<String>();
		for( Entry<String, TextFieldData> entry : textFields.entrySet() ) {
			names.add(entry.getKey());
		}
		return names;
	}
	public ArrayList<String> getTableNames() {
		ArrayList< String > names = new ArrayList<String>();
		for( Entry<String, TableData> entry : tables.entrySet() ) {
			names.add(entry.getKey());
		}
		return names;
	}

	
	public void dispose() {

	}

	public void setSkin( Skin skin ) { _skin = skin; } 
	public Skin getSkin() { return _skin; }

	public MyUIHelperType getHelper(String string) {
		return _allItems.get( string );
	}


	public TextButtonData getTextButtonData(String s) {return textButtons.get(s); }
	public TextFieldData getTextFieldData( String s ) { return textFields.get(s); }
	public LabelData getLabelData( String s ) { return labels.get(s); }
	public GroupData getGroupData( String s ) { return groups.get(s); }
	public TableData getTableData( String s ) { return tables.get(s); }


	public void printTree() {
		for( MyUIHelperType helper : _roots ) {
			printTreeHelper(helper, 0);
		}
		
	}
	private void printTreeHelper(MyUIHelperType root, int depth) {
		if( root == null )
			return;
		for( int i = 0; i < depth; ++i )
			System.out.print(" ");
		System.out.println( depth + ":" + root.name + "-" + root.type );
		
		for( int i = 0; i < root.children.size(); ++i ) {
			printTreeHelper( getHelper(root.children.get(i)), depth + 1);			
		}
		
	}

}
